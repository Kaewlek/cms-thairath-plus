module.exports = ({ env }) => ({
  upload: {
    provider: 'local-custom-path',
    providerOptions: {
      path: env('UPLOADS_PATH') || '.tmp/media',
      sizeLimit: 100000,
      
    },
  },
});