'use strict';

const { resolve } = require('path');
const range = require('koa-range');
const koaStatic = require('koa-static');
const get = require('lodash/get')

module.exports = strapi => ({
  initialize() {
    const configPublicPath = strapi.config.get(
      'middleware.settings.public.path',
      strapi.config.paths.static
    );

    const staticDir = get(strapi, "config.plugins.upload.providerOptions.path", resolve(strapi.dir, configPublicPath));

    strapi.app.on('error', err => {
      if (err.code === 'EPIPE') {
        return;
      }

      strapi.app.onerror(err);
    });

    strapi.router.get('/uploads/(.*)', range, koaStatic(staticDir, { defer: true }));
  },
});